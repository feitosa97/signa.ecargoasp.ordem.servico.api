﻿using Signa.EcargoAsp.Ordem.Servico.API.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Web;

namespace Signa.EcargoAsp.Ordem.Servico.API.Data.Repository
{
    public class SelectRepository : RepositoryBase, ISelectRepository
    {

        public List<object> Incoterm()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select TabTipoIncotermId = Tab_Tipo_Incoterm_Os_iD, TabTipoIncoterm = Desc_Tipo_Incoterm_Os From Tab_Tipo_Incoterm_Os Where Tab_Status_Id = 1  Order By 2").ToList();
            }
        }

        public List<object> PagadorOS()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select TabTipoPagadorOSId = Tab_Tipo_Pagador_Os_Id, TabTipoPagadorOS = Desc_Tipo_Pagador_OS From Tab_Tipo_Pagador_Os Where Tab_Status_Id = 1  Order By 2").ToList();
            }
        }

        public List<object> PontoOperacao()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select PontoOperacaoId = Ponto_Operacao_Id, PontoOperacao = Nome_Fantasia From vPonto_Operacao Where Tab_Status_Id = 1  Order By 2").ToList();
            }
        }

        public List<object> TipoOperacao()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select TabTipoOperacaoOSId = Tab_Tipo_Operacao_OS_Id, TabTipoOperacaoOS = Desc_Tipo_Operacao_OS From Tab_Tipo_Operacao_OS Where Tab_Status_Id = 1 Order By 2").ToList();
            }
        }
        
        public List<object> CentroCusto(int clienteId)
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select CentroCustoId = Centro_Custo_Cliente_ID, CentroCusto = Centro_Custo From vCentro_Custo_Cliente_Com_Acordo Where Tab_Status_Id = 1 And Cliente_Id = @clienteId  Order By 2", param: new { clienteId } ).ToList();
            }
        }

        public List<object> CentroCustoPrestador(int prestadorId)
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select CentroCustoPrestadorId = Centro_Custo_Cliente_Id, CentroCustoPrestador = Centro_Custo From vCentro_Custo_Com_Acordo_Fornec_Empresa Where Tab_Status_Id = 1 And Fornecedor_Id = @prestadorId  Order By 2", param: new { prestadorId }).ToList();
            }
        }

        public List<object> CargaIMO()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select CargaImoId = Tab_Tipo_Carga_Perigosa_Id, CargaImo = Psn From Tab_Tipo_Carga_Perigosa Where Tab_Status_Id = 1 Order By 2").ToList();
            }
        }

        public List<object> CargaAnvisa()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select CargaAnvisaId = Tab_Tipo_Carga_Anvisa_Id, CargaAnvisa = Desc_Tab_Tipo_Carga_Anvisa From Tab_Tipo_Carga_Anvisa Where Tab_Status_Id = 1 Order By 2").ToList();
            }
        }

        public List<object> TipoVeiculo()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select TabTipoVeiculo = Desc_Tipo_Veiculo, TabTipoVeiculoId = Tab_Tipo_Veiculo_Id From Tab_Tipo_Veiculo Where Tab_Status_Id = 1 Order By 1").ToList();
            }
        }

        public List<object> PeriodoColeta()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select TabFormaColetaId = Tab_Forma_Coleta_Id, TabFormaColeta = Desc_Tab_Forma_Coleta From Tab_Forma_Coleta Where Tab_Status_Id = 1 Order By 2").ToList();
            }
        }

        public List<object> TipoCntr()
        {
            using (var db = Connection)
            {
                return db.Query<object>("Select TipoCntr = Desc_Container, TipoCntrId = Tab_Tipo_Container_Id From Tab_Tipo_Container Where Tab_Status_Id = 1 Order By 1").ToList();
            }
        }

        
    }
}