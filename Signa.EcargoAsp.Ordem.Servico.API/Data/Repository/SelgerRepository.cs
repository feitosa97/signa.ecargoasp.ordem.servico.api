﻿using Signa.EcargoAsp.Ordem.Servico.API.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Web;

namespace Signa.EcargoAsp.Ordem.Servico.API.Data.Repository
{
    public class SelgerRepository : RepositoryBase, ISelgerRepository
    {
        public List<object> Cliente(string nome)
        {
            using (var db = Connection)
            {
                return db.Query<object>($"Select ClienteId = Pessoa_Id, Cliente = Nome_Fantasia From vCliente Where Tab_Status_Id = 1 And Nome_Fantasia Like @Nome + '%'").ToList();
            }
        }

        public List<object> Prestador(string nome)
        {
            using (var db = Connection)
            {
                return db.Query<object>($"Select PrestadorId = Pessoa_Id, Prestador = Nome_Fantasia From vFornecedor Where Tab_Status_Id = 1 And Nome_Fantasia Like @Nome + '%'").ToList();
            }
        }

        public List<object> TipoProduto(string nome)
        {
            using (var db = Connection)
            {
                return db.Query<object>($"Select TabTipoProduto = Desc_Produto, TabTipoProdutoId = Tab_Tipo_Produto_Id From Tab_Tipo_Produto Where Tab_Status_Id = 1 And Desc_Produto Like @Nome + '%' Order By Desc_Produto").ToList();
            }
        }

        public List<object> Cidade(string nome)
        {
            using (var db = Connection)
            {
                return db.Query<object>($"Select MunicipioId = Municipio_Id, Municipio = Municipio + ' - ' + Uf From Municipio Where Tab_Status_Id = 1 And Municipio Like @Nome + '%' Order By 2 ").ToList();
            }
        }
    }
}