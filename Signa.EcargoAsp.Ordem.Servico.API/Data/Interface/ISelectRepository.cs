﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signa.EcargoAsp.Ordem.Servico.API.Data.Interface
{
    interface ISelectRepository
    {
        List<object> PontoOperacao();
        List<object> Incoterm();
        List<object> PagadorOS();
        List<object> TipoOperacao();
        List<object> CentroCusto(int clienteId);
        List<object> CentroCustoPrestador(int prestadorId);
        List<object> CargaIMO();
        List<object> CargaAnvisa();
        List<object> TipoVeiculo();
        List<object> PeriodoColeta();
        List<object> TipoCntr();
    }
}
