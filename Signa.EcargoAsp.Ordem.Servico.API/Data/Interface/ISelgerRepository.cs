﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signa.EcargoAsp.Ordem.Servico.API.Data.Interface
{
    interface ISelgerRepository
    {
        List<object> Cliente(string nome);
        List<object> Prestador(string nome);
        List<object> TipoProduto(string nome);
        List<object> Cidade(string nome);
    }
}
