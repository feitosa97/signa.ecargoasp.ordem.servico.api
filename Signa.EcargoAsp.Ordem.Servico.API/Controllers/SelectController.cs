﻿using Signa.EcargoAsp.Ordem.Servico.API.Data.Interface;
using Signa.EcargoAsp.Ordem.Servico.API.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;


namespace Signa.EcargoAsp.Ordem.Servico.API.Controllers
{
    public class SelectController : ApiController
    {
        ISelectRepository _Select;

        SelectController()
        {
            _Select = new SelectRepository();
        }

        [HttpGet]
        [Route ("Select/PontoOperacao")]
        public IHttpActionResult PontoOperacao()
        {
            return Ok(_Select.PontoOperacao());
        }

        [HttpGet]
        [Route ("Select/Incoterm")]
        public IHttpActionResult Incoterm()
        {
            return Ok(_Select.Incoterm());
        }

        [HttpGet]
        [Route("Select/PagadorOS")]
        public IHttpActionResult PagadorOS()
        {
            return Ok(_Select.PagadorOS());
        }

        [HttpGet]
        [Route("Select/TipoOperacao")]
        public IHttpActionResult TipoOperacao()
        {
            return Ok(_Select.TipoOperacao());
        }

        [HttpGet]
        [Route("Select/CentroCusto")]
        public IHttpActionResult CentroCusto(int clienteId)
        {
            return Ok(_Select.CentroCusto(clienteId));
        }


        [HttpGet]
        [Route("Select/CentroCustoPrestador")]
        public IHttpActionResult CentroCustoPrestador(int prestadorId)
        {
            return Ok(_Select.CentroCustoPrestador(prestadorId));
        }

        [HttpGet]
        [Route ("Select/IMO")]
        public IHttpActionResult CargaIMO()
        {
            return Ok(_Select.CargaIMO());
        }

        [HttpGet]
        [Route("Select/Anvisa")]
        public IHttpActionResult CargaAnvisa()
        {
            return Ok(_Select.CargaAnvisa());
        }

        [HttpGet]
        [Route("Select/TipoVeiculo")]
        public IHttpActionResult TipoVeiculo()
        {
            return Ok(_Select.TipoVeiculo());
        }

        [HttpGet]
        [Route("Select/PeriodoColeta")]
        public IHttpActionResult PeriodoColeta()
        {
            return Ok(_Select.PeriodoColeta());
        }

        [HttpGet]
        [Route("Select/TipoCntr")]
        public IHttpActionResult TipoCntr()
        {
            return Ok(_Select.TipoCntr());
        }

    }
}
