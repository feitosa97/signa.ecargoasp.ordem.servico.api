﻿using Signa.EcargoAsp.Ordem.Servico.API.Data.Interface;
using Signa.EcargoAsp.Ordem.Servico.API.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Signa.EcargoAsp.Ordem.Servico.API.Controllers
{
    public class SelgerController : ApiController
    {
        ISelgerRepository _selger;

        SelgerController()
        {
            _selger = new SelgerRepository();
        }

        [HttpGet]
        [Route ("Selger/Cliente")]
        public IHttpActionResult Cliente(string q)
        {
            if (q.Length < 2)
                return Ok();
            return Ok(_selger.Cliente(q));
        }

        [HttpGet]
        [Route("Selger/Prestador")]
        public IHttpActionResult Prestador(string q)
        {
            if (q.Length < 2)
                return Ok();
            return Ok(_selger.Prestador(q));
        }

        [HttpGet]
        [Route("Selger/TipoProduto")]
        public IHttpActionResult TipoProduto(string q)
        {
            if (q.Length < 2)
                return Ok();
            return Ok(_selger.TipoProduto(q));
        }

        [HttpGet]
        [Route("Selger/Cidade")]
        public IHttpActionResult Cidade(string q)
        {
            if (q.Length < 2)
                return Ok();
            return Ok(_selger.Cidade(q));
        }
    }
}
